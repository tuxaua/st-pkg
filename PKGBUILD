# Maintainer: Klaus-Dieter Schmatz <tucuxir at sapo dot pt>
pkgname=st
pkgver=0.8.5
pkgrel=1
pkgdesc='A customized build of the Simple Terminal'
arch=('x86_64')
url='https://st.suckless.org'
license=('MIT')
depends=('libxft')
optdepends=()
source=("https://dl.suckless.org/$pkgname/$pkgname-$pkgver.tar.gz"
        "st-anysize-0.8.4.diff"
        "st-gruvbox-dark-0.8.5.diff"
        "st-ligatures-20210824-0.8.4.diff"
        "st-vertcenter-20180320-6ac8c8a.diff")
sha256sums=('ea6832203ed02ff74182bcb8adaa9ec454c8f989e79232cb859665e2f544ab37'
            '3851f7919e788cc6667ffdb28ca743188e2869a15f3fc34a8c0b39108d946ef0'
            '5b5a8c18945c7ef98ba7d857cc84881a8e97024fd0f3ca1a628303ea4004764b'
            'e414165d23663fd983621a71248cb9577434d07c2a0d5301b2047557e6b80756'
            '04e6a4696293f668260b2f54a7240e379dbfabbc209de07bd5d4d57e9f513360')
_sourcedir="$pkgname-$pkgver"
_makeopts="--directory=$_sourcedir"

prepare() {
  cat *.diff | patch --directory="$_sourcedir" --strip=1

  # This package provides a mechanism to provide a custom config.h. Multiple
  # configuration states are determined by the presence of two files in
  # $BUILDDIR:
  #
  # config.h  config.def.h  state
  # ========  ============  =====
  # absent    absent        Initial state. The user receives a message on how
  #                         to configure this package.
  # absent    present       The user was previously made aware of the
  #                         configuration options and has not made any
  #                         configuration changes. The package is built using
  #                         default values.
  # present                 The user has supplied his or her configuration. The
  #                         file will be copied to $srcdir and used during
  #                         build.
  #
  # After this test, config.def.h is copied from $srcdir to $BUILDDIR to
  # provide an up to date template for the user.
  if [ -e "$BUILDDIR/config.h" ]; then
    cp "$BUILDDIR/config.h" "$_sourcedir"
  elif [ ! -e "$BUILDDIR/config.def.h" ]; then
    msg='This package can be configured in config.h. Copy the config.def.h '
    msg+='that was just placed into the package directory to config.h and '
    msg+='modify it to change the configuration. Or just leave it alone to '
    msg+='continue to use default values.'
    echo "$msg"
  fi
  cp "$_sourcedir/config.def.h" "$BUILDDIR"
}

build() {
  make $_makeopts X11INC=/usr/include/X11 X11LIB=/usr/lib/X11
}

package() {
  local installopts='--mode 0644 -D --target-directory'
  licdir="$pkgdir/usr/share/licenses/$pkgname" 
  make $_makeopts PREFIX=/usr DESTDIR="$pkgdir" install
  install $installopts "$licdir" "$_sourcedir/LICENSE" 
}

# vim:set ts=2 sw=2 et:
